package ru.t1.sochilenkov.tm.command.system;

import ru.t1.sochilenkov.tm.api.service.ICommandService;
import ru.t1.sochilenkov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
