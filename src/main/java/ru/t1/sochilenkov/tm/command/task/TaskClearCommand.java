package ru.t1.sochilenkov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Clear all tasks.";

    public static final String NAME = "task-clear";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskService().clear();
    }

}
