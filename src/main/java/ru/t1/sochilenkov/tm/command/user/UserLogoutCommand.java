package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.api.service.IAuthService;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Logout from current user.";

    public static final String NAME = "user-logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final IAuthService authService = getAuthService();
        authService.logout();
    }

}
