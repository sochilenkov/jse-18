package ru.t1.sochilenkov.tm.command.task;

import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Complete task by index.";

    public static final String NAME = "task-complete-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.COMPLETED);
    }

}
