package ru.t1.sochilenkov.tm.command.project;

import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Show all projects.";

    public static final String NAME = "project-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName() + " | Status: " + project.getStatus().getDisplayName() + " |");
            index++;
        }
    }

}
