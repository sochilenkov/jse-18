package ru.t1.sochilenkov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Display author info.";

    public static final String NAME = "about";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikita Sochilenkov");
        System.out.println("E-mail: nsochilenkov@t1-consulting.ru");
    }

}
