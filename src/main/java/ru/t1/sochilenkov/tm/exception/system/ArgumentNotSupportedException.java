package ru.t1.sochilenkov.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
