package ru.t1.sochilenkov.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(String argument) {
        super("Error! Command ``" + argument + "`` not supported...");
    }

}
